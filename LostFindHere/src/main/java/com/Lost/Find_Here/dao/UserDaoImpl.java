package com.Lost.Find_Here.dao;

import java.io.Serializable;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.Lost.Find_Here.domain.Employee;
import com.Lost.Find_Here.domain.User;

public class UserDaoImpl implements UserDao {

	@Autowired
	SessionFactory sessionFactory;

	@Transactional
	public int insertRow(User user) {
		Session session = sessionFactory.openSession();
		// Transaction tx = session.beginTransaction();
		session.saveOrUpdate(user);
		// tx.commit();
		Serializable id = session.getIdentifier(user);
		session.close();
		return (Integer) id;
	}

	public User getUserByUserName(String username) {
		Session session = sessionFactory.openSession();
		Criteria crit = session.createCriteria(User.class);
		crit.add(Restrictions.eq("userName", username));
		@SuppressWarnings("unchecked")
		List<User> userList = crit.list();
		System.out.println("List Size: "+ userList.size());
		if(userList.size() > 0)
			return userList.get(0);
		return null;		
	}

	public List<User> getUserList() {
		Session session = sessionFactory.openSession();
		@SuppressWarnings("unchecked")
		List<User> userList = session.createCriteria(User.class).list();
		session.close();
		return userList;
	}

	public Employee getUserById(int id) {
		Session session = sessionFactory.openSession();
		Employee employee = (Employee) session.load(Employee.class, id);
		return employee;
	}

}
