package com.Lost.Find_Here.services;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.Lost.Find_Here.dao.UserDao;
import com.Lost.Find_Here.domain.User;

public class UserServiceImpl implements UserService {

	@Autowired
	UserDao userDao;

	public int insertRow(User user) {
		java.util.Date date = new java.util.Date();
		long time = date.getTime();
		Timestamp timeStamp = new Timestamp(time);
		user.setDateCreated(timeStamp);
		user.setDateUpdated(timeStamp);
		user.setLastLogin(timeStamp);
		System.out.println("Service:" + user);
		return userDao.insertRow(user);
	}

	public User getUserByUserName(String username) {
		return userDao.getUserByUserName(username);
	}

	public List<User> getUserList() {
		return userDao.getUserList();
	}
}
