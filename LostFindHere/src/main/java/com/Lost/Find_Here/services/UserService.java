package com.Lost.Find_Here.services;

import java.util.List;

import com.Lost.Find_Here.domain.User;

public interface UserService {

	 public int insertRow(User employee); 
	 
	 public User getUserByUserName(String username);
	 
	 public List<User> getUserList();
}
