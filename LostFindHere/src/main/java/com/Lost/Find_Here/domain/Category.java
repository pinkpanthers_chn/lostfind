package com.Lost.Find_Here.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "category")
public class Category {

	@Id
	@GeneratedValue
	private int id;

	@Column(name = "name")
	private String name;
	
	@OneToMany(cascade={CascadeType.ALL})
	@JoinColumn(name = "id")
    private Set<Item> itemList;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Item> getItemList() {
		return itemList;
	}

	public void setItemList(Set<Item> itemList) {
		this.itemList = itemList;
	}
	
	
}
