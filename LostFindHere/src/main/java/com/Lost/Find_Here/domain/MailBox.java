package com.Lost.Find_Here.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "mailbox")
public class MailBox {

	@Id
	@GeneratedValue
	private int id;

	@Column(name = "body")
	private String body;

	@ManyToOne
	@JoinColumn(name = "from_user", referencedColumnName = "username")
	private User fromUser;

	@ManyToOne
	@JoinColumn(name = "to_user", referencedColumnName = "username")
	private User toUser;

	@Column(name = "status")
	private char status;

	@Column(name = "isDeleted")
	private char isDeleted;

	@Column(name = "date_received")
	private Timestamp dateReceived;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public User getFromUser() {
		return fromUser;
	}

	public void setFromUser(User fromUser) {
		this.fromUser = fromUser;
	}

	public User getToUser() {
		return toUser;
	}

	public void setToUser(User toUser) {
		this.toUser = toUser;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	public char getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(char isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Timestamp getDateReceived() {
		return dateReceived;
	}

	public void setDateReceived(Timestamp dateReceived) {
		this.dateReceived = dateReceived;
	}

}
