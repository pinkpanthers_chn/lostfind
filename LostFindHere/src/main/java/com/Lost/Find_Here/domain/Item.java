package com.Lost.Find_Here.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "item")
public class Item {

	@Id
	@GeneratedValue
	private int id;

	@Column(name = "item_name")
	private String itemName;
	
	@Column(name = "location")
	private String location;
	
	@Column(name = "item_status")
	private char itemStatus;

	@Column(name = "entry_type")
	private char entryType;
	
	@ManyToOne
	@JoinColumn(name = "category_id",referencedColumnName = "id")
	private Category category;
	
	@ManyToOne
	@JoinColumn(name = "reported_by", referencedColumnName = "id")
	private User reportedBy;
	
	@OneToMany(cascade={CascadeType.ALL})
	@JoinColumn(name = "id")
    private Set<ItemDescription> itemDescriptionList;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public char getItemStatus() {
		return itemStatus;
	}

	public void setItemStatus(char itemStatus) {
		this.itemStatus = itemStatus;
	}

	public char getEntryType() {
		return entryType;
	}

	public void setEntryType(char entryType) {
		this.entryType = entryType;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public User getReportedBy() {
		return reportedBy;
	}

	public void setReportedBy(User reportedBy) {
		this.reportedBy = reportedBy;
	}
	
	
}
