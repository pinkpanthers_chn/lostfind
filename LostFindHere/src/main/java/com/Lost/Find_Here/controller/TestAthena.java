package com.Lost.Find_Here.controller;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class TestAthena {

	static int[] arr = { 1, 2, 3 };
	static int sum = 4;
	
	private static int sum(String[] arr) {
		Integer sum = 0;
		for(int i = 0; i< arr.length; i++)
			sum+=Integer.parseInt(arr[i]);
		return sum;			
	}
	
	private static void permutation1(String prefix, String str) {
	    int n = str.length();
	    
	    String[] arr = prefix.split(",");
	    
	    if (n == 0) {
	    	if(sum(arr) == sum) 
	    		System.out.println(prefix);
	    }
	    else {
	        for (int i = 0; i < n; i++) {
	        	System.out.println("permutation(" + prefix + str.charAt(i) + "," + str.substring(0,i) + str.substring(i+1,n) + ")");
	        	permutation(prefix + "," + arr[i], str.substring(0, i) + str.substring(i+1, n));
	        }    
	    }
	}
	
	private static void permutation(String prefix, String str) {
	    int n = str.length();
	    
	    if (n == 0) {
	    	System.out.println(prefix);
	    }
	    else {
	        for (int i = 0; i < n; i++) {
	        	System.out.println("permutation(" + prefix + str.charAt(i) + "," + str.substring(0,i) + str.substring(i+1,n) + ")");
	        	permutation(prefix + str.charAt(i), str.substring(0, i) + str.substring(i+1, n));
	        }    
	    }
	}

	public static void print(int num) {
		
		for (int i = 0; i < arr.length; i++) {

				//System.out.println("\nStartin for " + arr[i]);
				int count = 0, rem = 0;
				//System.out.println("num:" + num);
				count = num / arr[i];
				//System.out.println("count:" + count);
				if(count == 0) break;
				rem = num % (count * arr[i]);
				
				if (count == num) {
					System.out.println(" + " + count + " * " + arr[i]); //break;
				} else {
					System.out.print(count + " * " + arr[i]);
					//System.out.println("\ncalling print(" + rem + ")");
					print(rem);
				}
				System.out.println("");
		}
	}

	public static void main(String[] args) {

		/*for(int i = 0; i<arr.length; i++)
			print(sum);*/
		permutation1("", "123");
		//System.out.println("AB".substring(2,2));
	}

}
