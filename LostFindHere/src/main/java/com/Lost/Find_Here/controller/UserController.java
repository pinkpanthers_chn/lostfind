package com.Lost.Find_Here.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.Lost.Find_Here.domain.Item;
import com.Lost.Find_Here.domain.User;
import com.Lost.Find_Here.services.UserService;

@Controller
public class UserController {

	@Autowired
	UserService userService;

	@RequestMapping(value = "register", method = RequestMethod.GET)
	public ModelAndView getRegisterForm(@ModelAttribute User user) {
		return new ModelAndView("register");
	}

	@RequestMapping(value = "found", method = RequestMethod.GET)
	public ModelAndView foundForm(@ModelAttribute Item item,@RequestParam String from) {
		return new ModelAndView("found","from",from);
	}
	
	@RequestMapping(value = "registerUser", method = RequestMethod.POST)
	public ModelAndView registerUser(@ModelAttribute User user) {
		userService.insertRow(user);
		System.out.println(user);
		return new ModelAndView("redirect:listUsers");
	}
	
	@RequestMapping(value="loginUser", method = RequestMethod.POST)
	public String loginUser(@ModelAttribute User user) {
		//String password = user.getPassword();
		User user1 = userService.getUserByUserName(user.getUserName());
		if(user.getPassword().equals(user1.getPassword())) 
			return "home";
		return "register";
	}
	
	@RequestMapping(value = "isUserNameAvailable", method = RequestMethod.GET)
	public @ResponseBody String checkUserNameAvailable(@RequestParam String userName) {
		System.out.println("userName:"+userName);
		//System.out.println(userService.getUserByUserName(userName));
		if(userService.getUserByUserName(userName)!=null) {
			return "true";
		}
		return "false";
	}
	
	@RequestMapping("listUsers")
	public ModelAndView getList() {
		List<User> usersList = userService.getUserList();
		return new ModelAndView("list", "usersList", usersList);
	}

	@RequestMapping(value = "list/{userName}", method = RequestMethod.GET)
	public void getUserByUserName(@PathVariable String userName) {
		User user = userService.getUserByUserName(userName);
		System.out.println(user);
	}

}
