<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lost? Find Here | Registration Form</title>

<script type="text/javascript"
	src="<c:url value="/resources/jquery-2.1.3.js"/>"></script>

<script>
	$(document)
			.ready(
					function() {
						$('#userName')
								.on(
										"keyup change",
										function(event) {
											var userName = $('#userName').val();
											$
													.ajax({
														url : '<c:url value="/isUserNameAvailable"/>',
														type : "GET", //send it through get method
														data : ({
															userName : userName
														}),
														success : function(
																response) {
															if (response == "false") {
																$(
																		'#userNameError')
																		.css(
																				"display",
																				"none");
															} else {
																$(
																		'#userNameError')
																		.show();
															}
														},
														error : function(xhr) {
															//$('#userNameError').show();
														}
													});
										});
						$('#email')
								.on(
										"keydown",
										function() {
											var emailReg = new RegExp(
													/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i);
											var valid = emailReg.test($(
													'#email').val());

											if (!valid) {
												$('#emailError').show();
											} else {
												$('#emailError').css("display",
														"none");
											}
										});
						$('#cpwd').on(
								"change keydown keyup",
								function(event) { //event.type
									if ($('#password').val() != $('#cpwd')
											.val()) {
										$('#passwordError').show();
									} else if ($('#password').val() == $(
											'#cpwd').val()) {
										$('#passwordError').css("display",
												"none");
									}
								});
					});
</script>
</head>
<body>
	<center>

		<div style="color: teal; font-size: 30px">Lost? Find Here |
			Registration Form</div>

		<table>
			<tr>
				<td><form:form id="userRegisterForm" modelAttribute="user"
						method="post" action="registerUser">
						<table width="500px" height="150px">
							<tr>
								<td width="200px"><form:label path="firstName">First Name</form:label></td>
								<td colspan="2"><form:input path="firstName" /></td>
							</tr>
							<tr>
								<td><form:label path="lastName">Last Name</form:label></td>
								<td colspan="2"><form:input path="lastName" /></td>
							</tr>
							<tr>
								<td><form:label path="email">Email</form:label></td>
								<td><form:input id="email" path="email" /></td>
								<td><label id="emailError"
									style="color: red; display: none;"> Email error</label></td>
							</tr>
							<tr>
								<td><form:label path="userName">User Name</form:label></td>
								<td><form:input path="userName" /></td>
								<td><label id="userNameError"
									style="color: red; display: none;">UserName Exists</label>
							</tr>
							<tr>
								<td><form:label path="password">Password</form:label></td>
								<td colspan="2"><form:password id="password"
										path="password" /></td>
							</tr>
							<tr>
								<td>Confirm Password</td>
								<td><input type="password" id="cpwd" value="" name="cpwd" /></td>
								<td><label id="passwordError"
									style="color: red; display: none;"> Password mismatch</label></td>
							</tr>
							<tr>
								<td><form:label path="phoneNumber">Phone Number</form:label></td>
								<td colspan="2"><form:input path="phoneNumber" /></td>
							</tr>
							<tr>
								<td></td>
								<td colspan="3"><input type="submit" value="Register" /></td>
							</tr>
						</table>
					</form:form></td>
			</tr>
			<tr>
				<td><form:form id="userLoginForm" modelAttribute="user"
						method="post" action="loginUser">
						<table>
							<tr>
								<td><form:label path="userName">User Name</form:label></td>
								<td><form:input path="userName" /></td>
							</tr>
							<tr>
								<td><form:label path="password">Password</form:label></td>
								<td colspan="2"><form:password path="password" /></td>
							</tr>
							<tr>
								<td></td>
								<td colspan="3"><input type="submit" value="Login" /></td>
							</tr>
						</table>
					</form:form></td>
			</tr>
		</table>
		<a href="listUsers">Click Here to see User List</a>
	</center>
</body>
</html>
