<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Home Page</title>
</head>
<body>
	<center>

		<div style="color: teal; font-size: 30px">Lost? Find Here | User
			Home Page</div>

		<table align="center">
			<tr>
				<td><a href="found?from=Found">Found an Item..!</a></td>
			</tr>
			<tr>
				<td><a href="found?from=Lost">Lost an Item..?</a></td>
			</tr>
			<tr>
				<td><a href="wallet">User Wallet</a></td>
			</tr>
		</table>
	</center>
</body>
</html>